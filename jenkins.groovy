#!/usr/bin/env groovy
node() {

    try {
        stage('Prepare code') {
            git url: 'git clone https://cberdinas@bitbucket.org/cberdinas/040520.git', branch: 'master'
        }

        stage('Start Testing'){
            withMaven(maven: 'maven3.6.3') {
                sh "mvn clean test -Denvironment=dev    "
            }
        }

        currentBuild.result = "SUCCESS"
    } catch (e) {
        // If there was an exception thrown, the build failed
        currentBuild.result = "FAILED"
        throw e

    } finally {

        publishReport();
        //sendSlackNotification(currentBuild.result);

    }

}

def publishReport(){
    publishHTML(target: [
            reportName : 'Cucumber Report',
            reportDir:   'C:/Users/berdinas/repos/040520/target/cucumber/cucumber-html-reports',
            reportFiles: 'overview-features.html',
            keepAll:     true,
            alwaysLinkToLastBuild: true,
            allowMissing: false
    ])
}
